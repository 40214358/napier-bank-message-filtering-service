﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;

namespace NapierBankMessageFilteringService
{
    /// <summary>
    /// Interaction logic for EnterMessages.xaml
    /// </summary>
    public partial class EnterMessages : Page
    {
        public List<Message> messageList = new List<Message>();
        public EnterMessages()
        {
            InitializeComponent();
        }
        private void enterButton_Click(object sender, RoutedEventArgs e)
        {
            Message message = new Message();
            try
            {
                message.Header = messageHeaderTextBox.Text;
            }
            catch (Exception emptyString)
            {
                MessageBox.Show("An error has occured: " + emptyString.Message);
                return;
            }
            try
            {
                message.Body = messageBodyTextBox.Text;
            }
            catch (Exception emptyString)
            {
                MessageBox.Show("An error has occured: " + emptyString.Message);
                return;
            }
            if (message.Header.Substring(0, 1) == "S")
            {
                SMS textMessage = message as SMS;
                textMessage.Id = message.Header;
                textMessage.Sender = message.Body.Substring(0, 15);
                textMessage.MessageText = message.Body.Substring(15);
                lblMessageId.Content = textMessage.Id;
                lblMessageType.Content = "SMS";
                lblMessageSender.Content = textMessage.Sender;
                txtMessageText.Text = textMessage.MessageText;
                messageList.Add(textMessage);
            }
            else if (message.Header.Substring(0, 1) == "E")
            {
                string strRegex = @"[A-Za-z0-9_\-\+]+@[A-Za-z0-9\-]+\.([A-Za-z]{2,3})(?:\.[a-z]{2})?";
                Regex myRegex = new Regex(strRegex, RegexOptions.None);
                Match match = myRegex.Match(message.Body);
                Email emailMessage = new Email();
                emailMessage.Id = message.Header;
                emailMessage.Sender = match.ToString();
                emailMessage.Subject = message.Body.Substring(emailMessage.Sender.Length + 1, 20);
                emailMessage.MessageText = message.Body.Substring(emailMessage.Sender.Length + 21);
                lblMessageId.Content = emailMessage.Id;
                lblMessageType.Content = "Email";
                lblMessageSender.Content = emailMessage.Sender;
                lblMessageSubject.Content = emailMessage.Subject;
                txtMessageText.Text = emailMessage.MessageText;
                messageList.Add(emailMessage);
            }
            else if (message.Header.Substring(0, 1) == "T")
            {
                Tweet tweetMessage = new Tweet();
                tweetMessage.Id = message.Header;
                tweetMessage.Sender = message.Body.Split(' ').First();
                tweetMessage.TweetText = message.Body;
                lblMessageId.Content = tweetMessage.Id;
                lblMessageType.Content = "Tweet";
                lblMessageSender.Content = tweetMessage.Sender;
                txtMessageText.Text = tweetMessage.TweetText;
                messageList.Add(tweetMessage);
            }
       
        }

        private void btnMainMenu_Click(object sender, RoutedEventArgs e)
        {
            Menu newpage = new Menu(messageList);
            NavigationService.Navigate(newpage);
        }
    }
}

