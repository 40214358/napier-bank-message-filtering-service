﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Newtonsoft.Json;

namespace NapierBankMessageFilteringService
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Page
    {
        public List<Message> messageList;
        public Menu()
        {
            InitializeComponent();
        }
        public Menu(List<Message> m)
        {
            InitializeComponent();
            this.messageList = m;
        }

        private void btnEnterManually_Click(object sender, RoutedEventArgs e)
        {
            EnterMessages newpage = new EnterMessages();
            NavigationService.Navigate(newpage);
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            string json = JsonConvert.SerializeObject(messageList);
            System.IO.File.WriteAllText(@"\\psf\Home\Documents\Development\NapierBank\NapierBankMessageFilteringService\Messages.json", json);
            MessageBox.Show("Saved to Json file!");
        }
    }
}
