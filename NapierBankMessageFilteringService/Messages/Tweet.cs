﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NapierBankMessageFilteringService
{
    class Tweet : Message
    {
        private string id;
        private string sender;
        private string tweetText;

        public string Id
        {
            //get and set tweet id 
            get
            {
                return id;
            }
            set
            {
                //validation for the tweet id 
                if (String.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Tweet id cannot be left blank.");
                }
                id = value;
            }
        }

        public string Sender
        {
            //get and set SMS sender
            get
            {
                return sender;
            }
            set
            {
                //validation for the SMS sender 
                if (String.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("twitter id cannot be left blank.");
                }
                sender = value;
            }
        }
        public string TweetText
        {
            //get and set tweet text
            get
            {
                return tweetText;
            }
            set
            {
                //validation for the tweet text 
                if (String.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Tweet text cannot be left blank.");
                }
                tweetText = value;
            }
        }
    }
}
