﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NapierBankMessageFilteringService
{
    public class Message
    {
        protected string header;
        protected string body;
        
        public string Header
        {
            //get and set message header
            get
            {
                return header;
            }
            set
            {
                //validation for the message header 
                if (String.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Header cannot be left blank.");
                }
                if (value.Length != 10)
                {
                    throw new ArgumentException("Message id must comprise of a letter followed by 9 numeric characters.");
                }
                IList<string> strList = new List<string>() { "S", "E", "T" };
                if (!strList.Contains(value.Substring(0,1)))
                {
                    throw new ArgumentException("Message id must begin with 'S','E' or 'T'");
                }
                header = value;
            }
        }
        public string Body
        {
            //get and set message body
            get
            {
                return body;
            }
            set
            {
                //validation for the message body
                if (String.IsNullOrEmpty(value))
                    {
                        throw new ArgumentException("Body cannot be left blank.");
                    }
                body = value;
            }
        }
    }
}
