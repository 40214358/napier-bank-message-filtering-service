﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NapierBankMessageFilteringService
{
    class SMS : Message
    {
        private string id;
        private string sender;
        private string messageText;

        public string Id
        {
            //get and set SMS id 
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }

        public string Sender
        {
            //get and set SMS sender
            get
            {
                return sender;
            }
            set
            {
                //validation for the SMS sender 
                if (String.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Sender cannot be left blank.");
                }
                sender = value;
            }
        }
        public string MessageText
        {
            //get and set SMS message text
            get
            {
                return messageText;
            }
            set
            {
                //validation for the SMS message text 
                if (String.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("SMS text cannot be left blank.");
                }
                messageText = value;
            }
        }
    }
}
