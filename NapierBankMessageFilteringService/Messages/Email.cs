﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NapierBankMessageFilteringService
{
    class Email : Message
    {
        private string id;
        private string sender;
        private string subject;
        private string messageText;

        public string Id
        {
            //get and set Email id 
            get
            {
                return id;
            }
            set
            {
                //validation for the Email id 
                if (String.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Email id cannot be left blank.");
                }
                id = value;
            }
        }
        public string Subject
        {
            //get and set email subject
            get
            {
                return subject;
            }
            set
            {
                //validation for the Email subject 
                if (String.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Subject cannot be left blank.");
                }
                subject = value;
            }
        }
        public string Sender
        {
            //get and set Email sender
            get
            {
                return sender;
            }
            set
            {
                //validation for the Email sender 
                if (String.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Sender cannot be left blank.");
                }
                sender = value;
            }
        }
        public string MessageText
        {
            //get and set Email message text
            get
            {
                return messageText;
            }
            set
            {
                //validation for the Email message text 
                if (String.IsNullOrEmpty(value))
                {
                    throw new ArgumentException("Email text cannot be left blank.");
                }
                messageText = value;
            }
        }
    }
}
